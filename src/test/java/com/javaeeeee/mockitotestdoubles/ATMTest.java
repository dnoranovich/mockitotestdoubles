/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.mockitotestdoubles;

import com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Dmitry Noranovich
 */
@RunWith(MockitoJUnitRunner.class)
public class ATMTest {

    private ATM sut;
    @Mock
    private BankService bankService;
    @Mock
    private CashDispenser cashDispenser;
    @Mock
    private DepositSlot depositSlot;
    @Mock
    private Keypad keypad;
    @Mock
    private Screen screen;

    public ATMTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        sut = new ATM(bankService, cashDispenser, depositSlot, keypad, screen);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of isTransactionPossible, of class ATM.
     */
    @Test
    public void testIsTransactionPossible() {
        BigDecimal amount = new BigDecimal("1.1");
        BigDecimal balance = new BigDecimal("3.2");

        assertTrue("balance > amount",
                sut.isTransactionPossible(amount, balance));
        assertTrue("balance == amount",
                sut.isTransactionPossible(balance, balance));
        assertFalse("balance < amount",
                sut.isTransactionPossible(balance, amount));
    }

    /**
     * Test of withdrawCash, happy path, of class ATM. Static imports are not
     * used to stress what methods are from Mickito.
     *
     * @throws com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException
     */
    @Test
    public void testWithdrawCashSuccess() throws BankServiceException {
        //given
        BigDecimal amount = new BigDecimal("1000");
        BigDecimal balance = new BigDecimal("5000");
        Mockito.when(keypad.getAmount()).thenReturn(amount);
        Mockito.when(bankService.getBalance()).thenReturn(balance);
        Mockito.when(cashDispenser.isThereEnoughMoney(amount)).thenReturn(true);

        //when
        sut.withdrawCash();

        //then
        Mockito.verify(bankService, Mockito.times(1)).debit(amount);
        Mockito.verify(cashDispenser).dispenseCash(amount);
    }

    /**
     * Test of withdrawCash, sad path, of class ATM. BankServiceMethod throws an
     * exception. Static imports are not used to stress what methods are from
     * Mickito.
     *
     * @throws com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException
     */
    @Test(expected = BankServiceException.class)
    public void testWithdrawCashBankServiceFailure() throws BankServiceException {
        //given
        Mockito.when(bankService.getBalance())
                .thenThrow(new BankServiceException());

        //when
        sut.withdrawCash();
    }

    /**
     * Test of withdrawCash, sad path, of class ATM. In this case there is not
     * enough money on the user's account. Static imports are not used to stress
     * what methods are from Mickito.
     *
     * @throws com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException
     */
    @Test
    public void testWithdrawCashNotEnoughMoneyOnAccount() throws BankServiceException {
        //given
        BigDecimal amount = new BigDecimal("1000");
        BigDecimal balance = new BigDecimal("500");
        Mockito.when(keypad.getAmount()).thenReturn(amount);
        Mockito.when(bankService.getBalance()).thenReturn(balance);
        Mockito.when(cashDispenser.isThereEnoughMoney(amount)).thenReturn(true);

        //when
        sut.withdrawCash();

        //then
        Mockito.verifyZeroInteractions(cashDispenser);
        //http://stackoverflow.com/questions/1778744/using-mockitos-generic-any-method
        Mockito.verify(bankService, Mockito.times(0))
                .debit(Mockito.any(BigDecimal.class));
    }

    /**
     * Test of showBalance method, sad path, of class ATM. We stub the double to
     * throw an exception and check that the correct message was displayed/
     *
     * @throws com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException
     */
    @Test
    public void testShowBalance() throws BankServiceException {
        //given
        Mockito.when(bankService.getBalance())
                .thenThrow(new BankServiceException());

        //when
        sut.showBalance();

        //then
        Mockito.verify(screen).displayError(ATM.BANK_SERVICE_ERROR);
    }
}
