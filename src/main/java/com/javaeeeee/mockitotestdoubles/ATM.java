/*
 * The MIT License
 *
 * Copyright 2015 Dmitry Noranovich.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.mockitotestdoubles;

import com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException;
import java.math.BigDecimal;

/**
 * Class represents an Automatic Teller Machine (ATM).
 *
 * @author Dmitry Noranovich
 */
public class ATM {

    /**
     * Error message about insufficient cash in dispenser.
     */
    protected static final String INSUFFICIENT_CASH_AVAILABLE
            = "Insufficient Cash Available";
    /**
     * Error message about insufficient money on the account.
     */
    protected static final String AMOUNT_EXCEEDS_BALANCE
            = "Amount Exceeds Balance";
    /**
     * Final informational message.
     */
    protected static final String GOODBYE = "goodbye";

    /**
     * Bank Service error message.
     */
    protected static final String BANK_SERVICE_ERROR
            = "There is a problem connecting to the bank";

    /**
     * Bank service used to connect to bank to query information about account.
     */
    private final BankService bankService;
    /**
     * The cash dispenser of an ATM.
     */
    private final CashDispenser cashDispenser;
    /**
     * The slot to deposit cash of an ATM.
     */
    private final DepositSlot depositSlot;
    /**
     * The keypad of the ATM.
     */
    private final Keypad keypad;
    /**
     * The screen of the ATM.
     */
    private final Screen screen;

    /**
     * The constructor of the class.
     *
     * @param bankService Bank service used to connect to bank to query
     * information about account.
     * @param cashDispenser The cash dispenser of an ATM.
     * @param depositSlot The slot to deposit cash of an ATM.
     * @param keypad The keypad of the ATM.
     * @param screen The screen of the ATM.
     */
    public ATM(BankService bankService, CashDispenser cashDispenser,
            DepositSlot depositSlot, Keypad keypad, Screen screen) {
        this.bankService = bankService;
        this.cashDispenser = cashDispenser;
        this.depositSlot = depositSlot;
        this.keypad = keypad;
        this.screen = screen;
    }

    /**
     * Method allows user to view her account balance on the screen of an ATM.
     */
    public void showBalance() {
        try {
            BigDecimal balance = bankService.getBalance();
            screen.displayBalance(balance);
        } catch (BankServiceException e) {
            screen.displayError(BANK_SERVICE_ERROR);
        }
    }

    /**
     * A method to withdraw cash from an ATM.
     *
     * @throws com.javaeeeee.mockitotestdoubles.exceptions.BankServiceException
     */
    public void withdrawCash() throws BankServiceException {

        //Show user options to withdraw cash
        screen.displayMenuOfWithdrowalAmounts();

        //Read amount from the keypad
        BigDecimal amount = keypad.getAmount();

        //Connect to the bank and check balance
        BigDecimal balance = bankService.getBalance();

        //Check if there is enough money on the account
        if (isTransactionPossible(amount, balance)) {

            //Check if there is enough cash in the dispenser
            if (!cashDispenser.isThereEnoughMoney(amount)) {
                //Display error message
                screen.displayError(INSUFFICIENT_CASH_AVAILABLE);
            } else {

                //Debit amount from user's account
                bankService.debit(amount);

                //Dipenses cash
                cashDispenser.dispenseCash(amount);

                //Bids goodbye
                screen.displayMessage(GOODBYE);

            }
        } else {
            //Display error message
            screen.displayError(AMOUNT_EXCEEDS_BALANCE);
        }
    }

    /**
     * Compares amount to withdraw with user's account balance and decides if
     * cash can be dispensed.
     *
     * @param amount amount to be withdrawn
     * @param balance the balance of the user's account
     * @return true if balance is greater or equal to amount and false otherwise
     */
    protected Boolean isTransactionPossible(final BigDecimal amount,
            final BigDecimal balance) {
        return balance.compareTo(amount) >= 0;
    }
}
